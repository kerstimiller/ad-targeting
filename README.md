# ad-targeting

Assignment for Front End Developer 2022

## Technical requirements:

```
VueJS with vanilla JS (preferably ES6+).
Vanilla CSS, or a CSS preprocessor of your choice.
JS package manager of your choice.
The completed task should be published to any version control platform of your choice.
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
