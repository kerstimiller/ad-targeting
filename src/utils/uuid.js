export default (length = 8) => {
  let i, random;
  let uuid = "";
  for (i = 0; i < length; i++) {
    random = (Math.random() * 8) | 0;
    if (i === 4) {
      uuid += "-";
    }

    uuid += random.toString(16);
  }

  return uuid;
};
