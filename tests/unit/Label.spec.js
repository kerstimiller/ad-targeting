import { shallowMount } from "@vue/test-utils";
import Component from "../../src/components/Label.vue";

describe("Label.vue", () => {
  it("Renders `label` prop contents", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        label: "Label",
      },
    });

    expect(wrapper.element).toMatchSnapshot();
  });

  describe(`Modifier classes`, () =>
    it.each`
      propName   | propValue   | expectedModifier
      ${"color"} | ${"gray"}   | ${"--gray"}
      ${"color"} | ${"white"}  | ${"--white"}
      ${"color"} | ${"green"}  | ${"--green"}
      ${"color"} | ${"red"}    | ${"--red"}
      ${"color"} | ${"yellow"} | ${"--yellow"}
    `(
      '$expectedModifier is rendered for $propName="$propValue"',
      ({ propName, propValue, expectedModifier }) => {
        const wrapper = shallowMount(Component, {
          propsData: {
            [propName]: propValue,
          },
        });

        expect(wrapper.classes()).toContain(`ad-label${expectedModifier}`);
      }
    ));
});
