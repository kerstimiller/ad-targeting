import { shallowMount } from "@vue/test-utils";
import Component from "../../src/components/TargetItem.vue";

describe("TargetItem.vue", () => {
  it("Renders deletebutton", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        deleteButton: true,
      },
    });

    expect(wrapper.element).toMatchSnapshot();
  });

  it("Renders array of rules", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        rules: ["Video Games", "Sports"],
      },
    });

    expect(wrapper.element).toMatchSnapshot();
  });

  it("Renders type string", () => {
    const type = "Category";

    const wrapper = shallowMount(Component, {
      propsData: {
        type: "Category",
      },
    });

    expect(wrapper.text()).toBe(type);
  });

  it("Emits `clear` when clear button is clicked on tag", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        uuid: "0101-0101",
        type: "Country",
        rules: ["Estonia", "Turkey"],
      },
    });

    wrapper.find(".ad-target-item__clear").vm.$emit("clear");

    expect(wrapper.emitted("clear")).toBeTruthy();
  });

  it("Emits `delete` when delete button is clicked", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        uuid: "0101-0101",
        type: "Country",
        rules: ["Estonia", "Turkey"],
      },
    });

    wrapper.find(".ad-target-item__delete").vm.$emit("delete");

    expect(wrapper.emitted("delete")).toBeTruthy();
  });
});
