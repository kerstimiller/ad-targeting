import { shallowMount } from "@vue/test-utils";
import Component from "../../src/components/Tag.vue";

describe("Tag.vue", () => {
  it("Matches Snapshot of default render", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        name: "Estonia",
      },
    });

    expect(wrapper.element).toMatchSnapshot();
  });

  it("Emits `clear` when clear button is clicked and returns name", () => {
    const wrapper = shallowMount(Component, {
      propsData: {
        name: "Estonia",
      },
    });

    wrapper.find(".ad-tag__clear").vm.$emit("clear");
    expect(wrapper.emitted().clear[0][0]).toBe("Estonia");
  });
});
